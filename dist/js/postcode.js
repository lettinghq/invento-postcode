var postcode = angular.module('invento-postcode', []);

postcode.factory('InventoPostcode', function($resource){
    var postcode = {};

    // Defaults
    postcode.url    = 'https://api.postcod.es/v1/postcode/';
    postcode.apiKey = 'ceMb+cqPosJAkStQfl2wdAq+GuUF9McMT/BMztSGYyo=';

    postcode.valid = {
        address_line_one : true,
        address_line_two : true,
        town             : true,
        city             : true,
        county           : true,
        postcode         : true,
        number           : true,
    };

    postcode.address = {
        address_line_one : '',
        address_line_two : '',
        town             : '',
        city             : '',
        county           : '',
        postcode         : '',
        number           : '',
    };

    postcode.placeholders = {
        address_line_one : 'Street',
        address_line_two : 'Address Line Two',
        town             : 'Town',
        city             : 'City',
        county           : 'County',
        postcode         : 'Postcode',
        number           : 'House Name or Number',
        select_address   : 'Select Address'
    };

    // API request
    postcode.getAddress = function(){
        return $resource(this.url + ":postcode", {},{
            get : {
                method  : "GET",
                headers : {'api-key' : this.apiKey},
            }
        });
    };

    // Set a new address based on a API result
    postcode.setAddress = function(address){
        this.address.postcode         = address.postcode;
        this.address.address_line_one = address.street;
        this.address.town             = address.town;
        this.address.city             = address.city;
        this.address.county           = address.locality;
    };

    // Reset the API
    postcode.resetAddress = function(fullReset){
        this.address.address_line_one = '';
        this.address.address_line_two = '';
        this.address.town             = '';
        this.address.city             = '';
        this.address.county           = '';
        this.address.number           = '';
        
        if (fullReset !== false) {
            this.address.postcode = '';
        }
    };

    return postcode;
});

postcode.controller('InventoPostcode', function($scope, $rootScope, $localStorage, InventoPostcode){
    // Set the scope up to follow the factory
    $scope.address      = InventoPostcode.address;
    $scope.valid        = InventoPostcode.valid;
    $scope.placeholders = InventoPostcode.placeholders;

    // If there is no postcode we hide the other address fields
    if (!$scope.address.postcode) {
        $scope.hide_fields = true;
    }
    
    $scope.addressLookup = function(){
        $scope.invalid_postcode = false;
        // Make sure there is a postcode set before we start the lookup
        if (!$scope.address.postcode) {
            $scope.invalid_postcode = true;
            $scope.hide_fields      = true;
            InventoPostcode.resetAddress();
            return false;
        }

        $lookup = InventoPostcode.getAddress();
        $lookup.get({
            postcode : $scope.address.postcode
        }, function(success){
            data = success.response.data;
            // If the return result is 0 throw the error
            if (data.length <= 0 ) {
                $scope.invalid_postcode = true;
                $scope.hide_fields      = true;
                InventoPostcode.resetAddress(false);
                $scope.manualLookup();
                return false;
            } else if (data.length === 1) {
                InventoPostcode.setAddress(data[0]);
                $scope.hide_fields     = false;
            } else if (data.length > 1){
                $scope.show_options = true;
                $scope.addressOptions = data;
            }

        }, function(error){ 
            console.log(error);
        });
    };

    $scope.newLookup = function(){
        $scope.hide_fields      = true;
        InventoPostcode.resetAddress();
    };

    $scope.manualLookup = function(){
        $scope.hide_fields = false;
    };

    $scope.selectedAddress = function(address){
        if (address) {
            InventoPostcode.setAddress(address);
            $scope.hide_fields  = false;
            $scope.show_options = false;
        }
    };

});

postcode.directive('inventoPostcode', function(){
    return {
        template : '<div class="form-group" ng-hide="hide_fields">' +
                        '<div class="col-xs-12">' +
                            '<button type="button" class="btn btn-block btn-primary" ng-click="newLookup()">New Postcode Lookup</button>' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-show="hide_fields">' +
                        '<div class="col-xs-12">' +
                            '<button type="button" class="btn btn-block btn-primary" ng-click="manualLookup()">Manually Enter Address</button>' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-class="valid.number ? \'\' : \'has-error\' ">' +
                        '<label class="control-label col-xs-12 col-md-3">{{placeholders.number}}</label>' +
                        '<div class="col-xs-12 col-md-9">' +
                            '<input type="text" class="form-control input-size" ng-attr-placeholder="{{placeholders.number}}" ng-model="address.number">' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-show="show_options">' +
                        '<label class="control-label col-xs-12 col-md-3">{{placeholders.select_address}}</label>' +
                        '<div class="col-xs-12 col-md-9">' +
                            '<select class="form-control" ng-model="addressOption" ng-options="address.street + \', \' + address.city for address in addressOptions" ng-change="selectedAddress(addressOption)">' +
                                '<option value="">{{placeholders.select_address}}</option>' +
                            '</select>' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-hide="hide_fields" ng-class="valid.address_line_one ? \'\' : \'has-error\' ">' +
                        '<label class="control-label col-xs-12 col-md-3">{{placeholders.address_line_one}}</label>' +
                        '<div class="col-xs-12 col-md-9">' +
                            '<input type="text" class="form-control input-size" ng-attr-placeholder="{{placeholders.address_line_one}}" ng-model="address.address_line_one">' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-hide="hide_fields" ng-class="valid.address_line_two ? \'\' : \'has-error\' ">' +
                        '<label class="control-label col-xs-12 col-md-3">{{placeholders.address_line_two}}</label>' +
                        '<div class="col-xs-12 col-md-9">' +
                            '<input type="text" class="form-control input-size" ng-attr-placeholder="{{placeholders.address_line_two}}" ng-model="address.address_line_two">' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-hide="hide_fields" ng-class="valid.town ? \'\' : \'has-error\' ">' +
                        '<label class="control-label col-xs-12 col-md-3">{{placeholders.town}}</label>' +
                        '<div class="col-xs-12 col-md-9">' +
                            '<input type="text" class="form-control input-size" ng-attr-placeholder="{{placeholders.town}}" ng-model="address.town">' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-hide="hide_fields" ng-class="valid.city ? \'\' : \'has-error\' ">' +
                        '<label class="control-label col-xs-12 col-md-3">{{placeholders.city}}</label>' +
                        '<div class="col-xs-12 col-md-9">' +
                            '<input type="text" class="form-control input-size" ng-attr-placeholder="{{placeholders.city}}" ng-model="address.city">' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-hide="hide_fields" ng-class="valid.county ? \'\' : \'has-error\' ">' +
                        '<label class="control-label col-xs-12 col-md-3">{{placeholders.county}}</label>' +
                        '<div class="col-xs-12 col-md-9">' +
                            '<input type="text" class="form-control input-size" ng-attr-placeholder="{{placeholders.county}}" ng-model="address.county">' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group" ng-class="valid.postcode ? \'\' : \'has-error\' ">' + 
                        '<label class="control-label col-xs-12 col-md-3">{{placeholders.postcode}}</label>' +
                        '<div class="col-xs-12 col-md-6">' +
                            '<input type="text" class="form-control input-size" placeholder="{{placeholders.postcode}}" ng-model="address.postcode">' +
                            '<p class="help-block" ng-if="invalid_postcode"><span class="text-danger">We could not find this postcode, please fill in your address manually</span></p>' +
                        '</div>' +
                        '<div class="col-xs-12 col-md-3"><button type="button" class="btn btn-block btn-primary" ng-click="addressLookup()">Look Up</button></div>' +
                    '</div>',
    };
});
