A postcode lookup tool created by Invento.

Add the repo to your index.html file : <script src="bower_components/invento-postcode/dist/postcode.js"></script>

Add the module to your app : 'invento-postcode'

Create a run block in your root JS file to add your API key.

app.run(function(InventoPostcode)){
    InventoPostcode.apiKey = 'my-api-key-uuid-example';
}

Inject InventoPostcode into your controller.

Add a div to your HTML where you wish to display the invento-postcode inputs as follows : <div invento-postcode ng-controller="InventoPostcode"></div>

While invento-postcode does not do validation for you it is possible to send validation errors to invento-postcode to highlight required fields, set the required valid flag to false to highlight : InventoPostcode.valid.postcode = false .
Possible fields are address_line_one, address_line_two, town, city, county, postcode, number.

It is possible to change the default placeholders in invento-postcode by altering the InventoPostcode.placeholder object, you can change address_line_one, address_line_two, town, city, county, postcode, number, select_address fields.